package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "reports")
@XmlAccessorType
public class TrafficCameraReports {

    private List<TrafficCameraReport> traffic_camera_reports = null;
 
    @XmlElement(name = "report")
    public List<TrafficCameraReport> getReports() {
        return traffic_camera_reports;
    }
 
    public void setReports(List<TrafficCameraReport> reports) {
        traffic_camera_reports = reports;
    }	
}
