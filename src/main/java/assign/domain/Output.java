package assign.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "output")
public class Output {
	
	private String error = null;
	
	@XmlElement(name="error")
	public String getError() {
		return error;
	}
	
	public void setError(String camera_id) {
		error = "Traffic camera report with id " + camera_id + " does not exist";
	}
}