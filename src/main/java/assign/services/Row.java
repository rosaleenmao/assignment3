package assign.services;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "row")
public class Row {
	    private String camera_id;
    	private String location_name;
    	private String camera_status;
    	private String turn_on_date;
    	private String camera_mfg;
    	private String atd_location_id;
    	private String signal_eng_area; 
    	private String council_district;
    	private String jurisdiction_label; 
    	private String location_type; 
    	private String primary_st_block;
    	private String primary_st;
    	private String cross_st_block;
    	private String cross_st; 
    	private String coa_intersection_id;
    	private String modified_date;
    	private String ip_comm_status; 
    	private String comm_status_datetime_utc; 
    	private String location_longitude;
    	private String location_latitude;
    	private String screenshot_address;
    	private String funding;
    	private String id;
	    
    	//getters
    	@XmlElement(name="camera_id")
    	public String getCameraId() {
    		return camera_id;
    	}
    	
    	@XmlElement(name="location_name")
    	public String getLocationName() {
    		return location_name;
    	}

    	@XmlElement(name="camera_status")
    	public String getCameraStatus() {
    		return camera_status;
    	}

    	@XmlElement(name="turn_on_date")
    	public String getTurnOnDate() {
    		return turn_on_date;
    	}

    	@XmlElement(name="camera_mfg")
    	public String getCameraMfg() {
    		return camera_mfg;
    	}

    	@XmlElement(name="atd_location_id")
    	public String getAtdLocationId() {
    		return atd_location_id;
    	}

    	@XmlElement(name="signal_eng_area")
    	public String getSignalEngArea() {
    		return signal_eng_area;
    	}

    	@XmlElement(name="council_district")
    	public String getCouncilDistrict() {
    		return council_district;
    	}

    	@XmlElement(name="jurisdiction_label")
    	public String getJurisdictionLabel() {
    		return jurisdiction_label;
    	}

    	@XmlElement(name="location_type")
    	public String getLocationType() {
    		return location_type;
    	}

    	@XmlElement(name="primary_st_block")
    	public String getPrimaryStBlock() {
    		return primary_st_block;
    	}

    	@XmlElement(name="primary_st")
    	public String getPrimarySt() {
    		return primary_st;
    	}

    	@XmlElement(name="cross_st_block")
    	public String getCrossStBlock() {
    		return cross_st_block;
    	}

    	@XmlElement(name="cross_st")
    	public String getCrossSt() {
    		return cross_st;
    	}

    	@XmlElement(name="coa_intersection_id")
    	public String getCoaIntersectionId() {
    		return coa_intersection_id;
    	}

    	@XmlElement(name="modified_date")
    	public String getModifiedDate() {
    		return modified_date;
    	}

    	@XmlElement(name="ip_comm_status")
    	public String getIpCommStatus() {
    		return ip_comm_status;
    	}

    	@XmlElement(name="comm_status_datetime_utc")
    	public String getCommStatusDatetimeUtc() {
    		return comm_status_datetime_utc;
    	}

    	@XmlElement(name="location_longitude")
    	public String getLocationLongitude() {
    		return location_longitude;
    	}

    	@XmlElement(name="location_latitude")
    	public String getLocationLatitude() {
    		return location_latitude;
    	}

    	@XmlElement(name="screenshot_address")
    	public String getScreenshotAddress() {
    		return screenshot_address;
    	}

    	@XmlElement(name="funding")
    	public String getFunding() {
    		return funding;
    	}

    	@XmlElement(name="id")
    	public String getId() {
    		return id;
    	}
    	
    	//setters
    	public void setCameraId(String input) {
    		camera_id = input;
    	}
    	
    	public void setLocationName(String input) {
    		location_name = input;
    	}
    	
    	public void setCameraStatus(String input) {
    		camera_status = input;
    	}
    	
    	public void setTurnOnDate(String input) {
    		turn_on_date = input;
    	}
    	
    	public void setCameraMfg(String input) {
    		camera_mfg = input;
    	}
    	
    	public void setAtdLocationId(String input) {
    		atd_location_id = input;
    	}
    	
    	public void setSignalEngArea(String input) {
    		signal_eng_area = input;
    	}
    	
    	public void setCouncilDistrict(String input) {
    		council_district = input;
    	}
    	
    	public void setJurisdictionLabel(String input) {
    		jurisdiction_label = input;
    	}
    	
    	public void setLocationType(String input) {
    		location_type = input;
    	}
    	
    	public void setPrimaryStBlock(String input) {
    		primary_st_block = input;
    	}
    	
    	public void setPrimarySt(String input) {
    		primary_st = input;
    	}
    	
    	public void setCrossStBlock(String input) {
    		cross_st_block = input;
    	}
    	
    	public void setCrossSt(String input) {
    		cross_st = input;
    	}
    	
    	public void setCoaIntersectionId(String input) {
    		coa_intersection_id = input;
    	}
    	
    	public void setModifiedDate(String input) {
    		modified_date = input;
    	}
    	
    	public void setIpCommStatus(String input) {
    		ip_comm_status = input;
    	}
    	
    	public void setCommStatusDatetimeUtc(String input) {
    		comm_status_datetime_utc = input;
    	}
    	
    	public void setLocationLongitude(String input) {
    		location_longitude = input;
    	}
    	
    	public void setLocationLatitude(String input) {
    		location_latitude = input;
    	}
    	
    	public void setScreenshotAddress(String input) {
    		screenshot_address = input;
    	}
    	
    	public void setFunding(String input) {
    		funding = input;
    	}
    	
    	public void setId(String input) {
    		id = input;
    	}
    }