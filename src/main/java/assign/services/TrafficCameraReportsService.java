package assign.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import assign.domain.TrafficCameraReport;
import assign.domain.TrafficCameraReports;

public class TrafficCameraReportsService {
	
	public TrafficCameraReports getReports() throws JAXBException, MalformedURLException, IOException {
		ArrayList<TrafficCameraReport> reportList = new ArrayList<TrafficCameraReport>();
	    JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	     
	    Response cameraNodes = (Response) jaxbUnmarshaller.unmarshal( 
	    		new URL("http://www.cs.utexas.edu/~devdatta/traffic_camera_data.xml").openStream());
    	for(Row camera : cameraNodes.getRowList()) {
    		TrafficCameraReport report = new TrafficCameraReport();
    		report.setCameraId(camera.getCameraId());
    		reportList.add(report);
	    }
    	
    	TrafficCameraReports allReports = new TrafficCameraReports();
    	allReports.setReports(reportList);
	    return allReports;
	}
	
	public TrafficCameraReport getReport(String camera_id) throws JAXBException, MalformedURLException, IOException {
	    JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	     
	    Response cameraNodes = (Response) jaxbUnmarshaller.unmarshal( 
	    		new URL("http://www.cs.utexas.edu/~devdatta/traffic_camera_data.xml").openStream());
    	for(Row camera : cameraNodes.getRowList()) {
    		if (camera.getCameraId().equals(camera_id)) {
    			TrafficCameraReport report = createReport(camera);
    			return report;
    		}
	    }
	    return null;
	}
	
	private TrafficCameraReport createReport(Row camera) {
		TrafficCameraReport report = new TrafficCameraReport();
		
		//set all fields
		report.setCameraId(camera.getCameraId());
		report.setLocationName(camera.getLocationName());
		report.setCameraStatus(camera.getCameraStatus());
		report.setTurnOnDate(camera.getTurnOnDate());
		report.setCameraMfg(camera.getCameraMfg());
		report.setAtdLocationId(camera.getAtdLocationId());
		report.setSignalEngArea(camera.getSignalEngArea());
		report.setCouncilDistrict(camera.getCouncilDistrict());
		report.setJurisdictionLabel(camera.getJurisdictionLabel());
		report.setLocationType(camera.getLocationType());
		report.setPrimaryStBlock(camera.getPrimaryStBlock());
		report.setPrimarySt(camera.getPrimarySt());
		report.setCrossStBlock(camera.getCrossStBlock());
		report.setCrossSt(camera.getCrossSt());
		report.setCoaIntersectionId(camera.getCoaIntersectionId());
		report.setModifiedDate(camera.getModifiedDate());
		report.setIpCommStatus(camera.getIpCommStatus());
		report.setCommStatusDatetimeUtc(camera.getCommStatusDatetimeUtc());
		report.setLocationLongitude(camera.getLocationLongitude());
		report.setLocationLatitude(camera.getLocationLatitude());
		report.setScreenshotAddress(camera.getScreenshotAddress());
		report.setFunding(camera.getFunding());
		report.setId(camera.getId());
		
		return report;
	}
}
