package assign.resources;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.Output;
import assign.domain.TrafficCameraReport;
import assign.domain.TrafficCameraReports;
import assign.services.TrafficCameraReportsService;

@Path("/")
public class TrafficCameraReportsResource {
	
	TrafficCameraReportsService trafficCameraReportsService;
	
	public TrafficCameraReportsResource() {
		// Dependency Inject not used.
		trafficCameraReportsService = new TrafficCameraReportsService();
	}
	
	public void setTrafficCameraReportsService(TrafficCameraReportsService service) {
		trafficCameraReportsService = service;
	}
	
	@GET
	@Path("/")
	@Produces("text/html")
	public String welcome() {
		return "Welcome to Traffic Camera Reports REST API"
				+ "<br>For all reports use: ​http://localhost:8080/assignment3/trafficcamerareports/reports"
				+ "<br>For a specific report use:"
				+ "<br>http://localhost:8080/assignment3/trafficcamerareports/reports/{camera_id}";
	}	
	
	@GET
	@Path("/reports")
	@Produces("application/xml")
	public StreamingOutput getReports() throws Exception {
		final TrafficCameraReports reports = trafficCameraReportsService.getReports();
			    
	    return new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputReports(outputStream, reports);
	         }
	      };	    
	}
	
	@GET
	@Path("/reports/{camera_id}")
	@Produces("application/xml")
	public StreamingOutput getReport(@PathParam("camera_id") String camera_id) throws Exception {
		final TrafficCameraReport report = trafficCameraReportsService.getReport(camera_id);	
		
		if (report == null) {
			final Output error = new Output();
			error.setError(camera_id);
			return new StreamingOutput() {
				public void write(OutputStream outputStream) throws IOException, WebApplicationException {
					outputError(outputStream, error);
				}
			};
		} else {
			return new StreamingOutput() {
				public void write(OutputStream outputStream) throws IOException, WebApplicationException {
					outputReport(outputStream, report);
				}
			};
		}   
	}		
	
	protected void outputReports(OutputStream os, TrafficCameraReports reports) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(TrafficCameraReports.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(reports, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	protected void outputReport(OutputStream os, TrafficCameraReport report) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(TrafficCameraReport.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(report, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	protected void outputError(OutputStream os, Output error) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Output.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(error, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
}