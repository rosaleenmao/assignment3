package assign.resources;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TestDOMParsers {

	// finds and returns an ArrayList of all camera_ids from a certain url
	// adapted from prof's code
	public static ArrayList<String> getReportsDOMParser(String url) {
		ArrayList<String> allCameraIds = new ArrayList<String>();

		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = builderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document document = null;
		try {
			document = builder.parse(new URL(url).openStream());
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Element rootElement = document.getDocumentElement();
		Queue<Element> q = new LinkedList<Element>();
		q.add(rootElement);

		while (!q.isEmpty()) {
			Element e = (Element) q.remove();
			String nodeName = e.getNodeName().toLowerCase();
			if (nodeName.equals("camera_id")) {
				String nodeValue = e.getTextContent();
				allCameraIds.add(nodeValue);
			}
			NodeList nodes = e.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (node instanceof Element) {
					q.add((Element) node);
				}
			}
		}
		return allCameraIds;
	}

	// finds and returns a HashMap of the parameters and their values from our data
	public static HashMap<String, String> getReportDOMParserOurXML(String camera_id) {
		HashMap<String, String> allCameraIds = new HashMap<String, String>();

		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = builderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document document = null;
		try {
			String url = "http://localhost:8080/assignment3/trafficcamerareports/reports/" + camera_id;
			document = builder.parse(new URL(url).openStream());
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Element rootElement = document.getDocumentElement();
		Queue<Element> q = new LinkedList<Element>();
		q.add(rootElement);

		// find parameters and store values in HashMap
		while (!q.isEmpty()) {
			Element e = (Element) q.remove();
			String nodeName = e.getNodeName().toLowerCase();
			if (nodeName.equals("camera_id")) {
				String nodeValue = e.getTextContent();
				allCameraIds.put("camera_id", nodeValue);
			}
			if (nodeName.equals("camera_status")) {
				String nodeValue = e.getTextContent();
				allCameraIds.put("camera_status", nodeValue);
			}
			if (nodeName.equals("camera_mfg")) {
				String nodeValue = e.getTextContent();
				allCameraIds.put("camera_mfg", nodeValue);
			}
			if (nodeName.equals("ip_comm_status")) {
				String nodeValue = e.getTextContent();
				allCameraIds.put("ip_comm_status", nodeValue);
			}
			if (allCameraIds.size() == 4) {
				return allCameraIds;
			}
			NodeList nodes = e.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (node instanceof Element) {
					q.add((Element) node);
				}
			}
		}
		return null;
	}

	public static HashMap<String, String> getReportDOMParserOfficialXML(String camera_id) {
		HashMap<String, String> allCameraIds = new HashMap<String, String>();

		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = builderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		Document document = null;
		try {
			document = builder
					.parse(new URL("http://www.cs.utexas.edu/~devdatta/traffic_camera_data.xml").openStream());
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Element rootElement = document.getDocumentElement();
		Queue<Element> q = new LinkedList<Element>();
		q.add(rootElement);

		boolean cameraFound = false;

		// we know camera id always comes first
		// so once we find the right camera, we can just look for the other parameters
		while (!q.isEmpty()) {
			Element e = (Element) q.remove();
			String nodeName = e.getNodeName().toLowerCase();
			if (nodeName.equals("camera_id")) {
				String nodeValue = e.getTextContent();
				// check to see if this is the right camera
				if (nodeValue.equals(camera_id)) {
					allCameraIds.put("camera_id", nodeValue);
					cameraFound = true;
				}
			}
			if (nodeName.equals("camera_status") && cameraFound) {
				String nodeValue = e.getTextContent();
				allCameraIds.put("camera_status", nodeValue);
			}
			if (nodeName.equals("camera_mfg") && cameraFound) {
				String nodeValue = e.getTextContent();
				allCameraIds.put("camera_mfg", nodeValue);
			}
			if (nodeName.equals("ip_comm_status") && cameraFound) {
				String nodeValue = e.getTextContent();
				allCameraIds.put("ip_comm_status", nodeValue);
			}
			if (allCameraIds.size() == 4) {
				return allCameraIds;
			}
			NodeList nodes = e.getChildNodes();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (node instanceof Element) {
					q.add((Element) node);
				}
			}
		}
		return null;
	}

}
