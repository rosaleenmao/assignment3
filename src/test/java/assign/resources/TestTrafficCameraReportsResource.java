package assign.resources;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;

import assign.domain.TrafficCameraReport;
import assign.domain.TrafficCameraReports;
import assign.services.TrafficCameraReportsService;

public class TestTrafficCameraReportsResource {

	private TrafficCameraReportsResource mockResource;
	private TrafficCameraReportsService mockService;
	private TrafficCameraReportsService realService;

	@Before
	public void initTest() {
		mockResource = new TrafficCameraReportsResource();
		mockService = mock(TrafficCameraReportsService.class);
		mockResource.setTrafficCameraReportsService(mockService);
		realService = new TrafficCameraReportsService();
	}

	@Test
	public void unitTestGetReport() throws MalformedURLException, JAXBException, IOException {
		System.out.println("**** Unit Testing getReport ***");

		// set up expected output

		TrafficCameraReport mockReport = new TrafficCameraReport();
		mockReport.setAtdLocationId("LOC16-004705");
		mockReport.setCameraId("12");
		mockReport.setCameraMfg("Sarix");
		mockReport.setCameraStatus("TURNED_ON");
		mockReport.setCoaIntersectionId("5168394");
		mockReport.setCommStatusDatetimeUtc("1559810100000");
		mockReport.setCouncilDistrict("2");
		mockReport.setCrossSt(" SAINT THOMAS DR");
		mockReport.setCrossStBlock("12818");
		mockReport.setFunding("None Identified");
		mockReport.setId("582c83276703af1e2d14d77b");
		mockReport.setIpCommStatus("OFFLINE");
		mockReport.setJurisdictionLabel("AUSTIN 2 MILE ETJ");
		mockReport.setLocationLatitude("30.1783047");
		mockReport.setLocationLongitude("-97.6151962");
		mockReport.setLocationName(" ROSS RD / SAINT THOMAS DR");
		mockReport.setLocationType("ROADWAY");
		mockReport.setModifiedDate("1560785820000");
		mockReport.setPrimarySt(" ROSS RD");
		mockReport.setPrimaryStBlock("4500");
		mockReport.setScreenshotAddress(" http://atdatmsweb2.austintexas.gov/CCTVImages/CCTV12.jpg");
		mockReport.setSignalEngArea("SOUTHEAST");
		mockReport.setTurnOnDate("1479276000000");

		TrafficCameraReportsResource reportsResource = new TrafficCameraReportsResource();
		reportsResource.setTrafficCameraReportsService(mockService);
		when(mockService.getReport("12")).thenReturn(mockReport);

		// do the test comparing expected output and our service class' outputs

		assertEquals(mockService.getReport("12").getCameraId(), realService.getReport("12").getCameraId());
		assertEquals(mockService.getReport("12").getAtdLocationId(), realService.getReport("12").getAtdLocationId());
		assertEquals(mockService.getReport("12").getCameraMfg(), realService.getReport("12").getCameraMfg());
		assertEquals(mockService.getReport("12").getCameraStatus(), realService.getReport("12").getCameraStatus());
		assertEquals(mockService.getReport("12").getCoaIntersectionId(),
				realService.getReport("12").getCoaIntersectionId());
		assertEquals(mockService.getReport("12").getCommStatusDatetimeUtc(),
				realService.getReport("12").getCommStatusDatetimeUtc());
		assertEquals(mockService.getReport("12").getCouncilDistrict(),
				realService.getReport("12").getCouncilDistrict());
		assertEquals(mockService.getReport("12").getCrossSt(), realService.getReport("12").getCrossSt());
		assertEquals(mockService.getReport("12").getCrossStBlock(), realService.getReport("12").getCrossStBlock());
		assertEquals(mockService.getReport("12").getFunding(), realService.getReport("12").getFunding());
		assertEquals(mockService.getReport("12").getId(), realService.getReport("12").getId());
		assertEquals(mockService.getReport("12").getIpCommStatus(), realService.getReport("12").getIpCommStatus());
		assertEquals(mockService.getReport("12").getJurisdictionLabel(),
				realService.getReport("12").getJurisdictionLabel());
		assertEquals(mockService.getReport("12").getLocationLatitude(),
				realService.getReport("12").getLocationLatitude());
		assertEquals(mockService.getReport("12").getLocationLongitude(),
				realService.getReport("12").getLocationLongitude());
		assertEquals(mockService.getReport("12").getLocationName(), realService.getReport("12").getLocationName());
		assertEquals(mockService.getReport("12").getLocationType(), realService.getReport("12").getLocationType());
		assertEquals(mockService.getReport("12").getModifiedDate(), realService.getReport("12").getModifiedDate());
		assertEquals(mockService.getReport("12").getPrimarySt(), realService.getReport("12").getPrimarySt());
		assertEquals(mockService.getReport("12").getPrimaryStBlock(), realService.getReport("12").getPrimaryStBlock());
		assertEquals(mockService.getReport("12").getScreenshotAddress(),
				realService.getReport("12").getScreenshotAddress());
		assertEquals(mockService.getReport("12").getSignalEngArea(), realService.getReport("12").getSignalEngArea());
		assertEquals(mockService.getReport("12").getTurnOnDate(), realService.getReport("12").getTurnOnDate());

		System.out.println("**** Unit Test getReport passed ***");
	}

	@Test
	public void unitTestGetReports() throws MalformedURLException, JAXBException, IOException {
		System.out.println("**** Unit Testing getReports ***");

		// set up expected output using a DOM parser to look at the given data

		ArrayList<String> expectedReports = TestDOMParsers.getReportsDOMParser(
				"http://www.cs.utexas.edu/~devdatta/traffic_camera_data.xml");

		// set up real output

		TrafficCameraReports realReportsObject = realService.getReports();
		List<TrafficCameraReport> realReports = realReportsObject.getReports();

		// compare each element of the two lists to ensure they are the same and make
		// sure they're the same size

		assertEquals(expectedReports.size(), realReports.size());

		for (int i = 0; i < expectedReports.size(); i++) {
			assertEquals(expectedReports.get(i), realReports.get(i).getCameraId());
			i++;
		}

		System.out.println("**** Unit Test getReports passed ***");
	}

	@Test
	public void functionalTestGetReport() {
		System.out.println("**** Functional Testing getReports ***");

		// set up expected output using a DOM parser to look at the given data

		HashMap<String, String> expectedValues = TestDOMParsers.getReportDOMParserOfficialXML("12");

		// set up real output using a DOM parser to look at the data we've generated

		HashMap<String, String> realValues = TestDOMParsers.getReportDOMParserOurXML("12");

		// compare each element of the two lists to ensure they are the same

		for (String param : expectedValues.keySet()) {
			assertEquals(expectedValues.get(param), realValues.get(param));
		}

		System.out.println("**** Functional Test getReports passed ***");

	}

	@Test
	public void functionalTestGetReports() {
		System.out.println("**** Functional Testing getReports ***");

		// set up expected output using a DOM parser to look at the given data

		ArrayList<String> expectedReports = TestDOMParsers.getReportsDOMParser(
				"http://www.cs.utexas.edu/~devdatta/traffic_camera_data.xml");

		// set up real output using a DOM parser to look at the data we've generated

		ArrayList<String> realReports = TestDOMParsers.getReportsDOMParser(
				"http://localhost:8080/assignment3/trafficcamerareports/reports");

		// compare each element of the two lists to ensure they are the same and make
		// sure they're the same size

		assertEquals(expectedReports.size(), realReports.size());

		for (int i = 0; i < expectedReports.size(); i++) {
			assertEquals(expectedReports.get(i), realReports.get(i));
			i++;
		}

		System.out.println("**** Functional Test getReports passed ***");

	}
}